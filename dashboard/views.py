from django.shortcuts import render
from works.models import VacantTopic


def index(request):
    return render(request, 'index.html', {'random_vacant_topic': VacantTopic.objects.order_by('?').first()})
