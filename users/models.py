from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


class UserProfile(models.Model):
    STUDENT = 'ST'
    SUPERVISOR = 'SV'
    USER_TYPES = [(STUDENT, 'Student'),
                  (SUPERVISOR, 'Supervisor')]

    user = models.OneToOneField(User, related_name='user_profile')
    type = models.CharField(choices=USER_TYPES, max_length=2, default=STUDENT)
    first_name = models.CharField(max_length=50, default='')
    last_name = models.CharField(max_length=50, default='')
    avatar = models.ImageField(upload_to='avatars', blank=True, default='avatars/default_avatar.png')

    def __str__(self):
        if self.first_name or self.last_name:
            return '%s %s' % (self.first_name, self.last_name)
        else:
            return str(self.pk)


@receiver(post_save, sender=User)
def create_catalog(sender, instance, *args, **kwargs):
    UserProfile.objects.get_or_create(user=instance)
