from django import forms


class ProfileForm(forms.Form):
    first_name = forms.CharField(label='First name', max_length=50, required=False)
    last_name = forms.CharField(label='Second name', max_length=50, required=False)
    avatar = forms.FileInput()
