from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect

from users.forms import ProfileForm


def login_view(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            messages.success(request, 'Logged in successfully.')
            return redirect('index')
        else:
            messages.warning(request, 'Account isn\'t active.')
            return redirect('index')
    else:
        messages.error(request, 'Wrong username/password combination.')
        return redirect('index')


def logout_view(request):
    logout(request)
    messages.info(request, 'Logged out successfully.')
    return redirect('index')


def change_profile(request):
    if request.method == 'POST':
        form = ProfileForm(request.POST, request.FILES)
        if form.is_valid():
            user_profile = request.user.user_profile
            user_profile.first_name = request.POST['first_name']
            user_profile.last_name = request.POST['last_name']
            if 'avatar' in request.FILES:
                user_profile.avatar = request.FILES['avatar']
            user_profile.save()
        else:
            # TODO: error handling
            print form.errors
    return redirect('index')
