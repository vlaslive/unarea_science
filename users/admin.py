from django.contrib import admin
from users.models import UserProfile


class UserProfileAdmin(admin.ModelAdmin):
    exclude = ('user',)

admin.site.register(UserProfile, UserProfileAdmin)
