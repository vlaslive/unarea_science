from django.http import JsonResponse, QueryDict
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View

from works.forms import *
from works.models import *
from users.models import UserProfile


def add_vacant_topic(request):
    response = {'valid': True, 'message': ''}
    if request.method == 'POST':
        form = VacantTopicForm(request.POST)
        if form.is_valid() and request.user.user_profile.type == UserProfile.SUPERVISOR:
            vacant_topic = VacantTopic(subject=request.POST['subject'], user=request.user)
            year = 0
            if request.POST['year']:
                year = request.POST['year']
            vacant_topic.year = year
            vacant_topic.annotation = request.POST['annotation']
            if 'for_student' in request.POST and request.POST['for_student'] and\
                    User.objects.filter(id=request.POST['for_student']).exists():
                vacant_topic.for_student_id = request.POST['for_student']
            vacant_topic.save()
        else:
            response['valid'] = False
            response['errors'] = {}
            for key, value in form.errors.iteritems():
                response['errors'][key] = [error for error in value]
    else:
        response['valid'] = False
        response['message'] = 'Method not allowed.'
    return JsonResponse(response)


def vacant_students(request):
    response = {'response': []}
    for student in UserProfile.objects.filter(first_name__icontains=request.GET['q'],
                                              last_name__icontains=request.GET['q'],
                                              type=UserProfile.STUDENT):
        response['response'].append({'name': '%s' % student, 'id': student.id})
    return JsonResponse(response)


class VacantTopicPage(View):
    template_name = 'vacant_topic.html'

    def get(self, request, pk, *args, **kwargs):
        return render(request, self.template_name, {'vacant_topic': VacantTopic.objects.get(id=pk)})


def add_application(request, pk):
    response = {'valid': True, 'message': ''}
    if request.method == 'POST':
        form = ApplicationForm(request.POST)
        if form.is_valid():
            if Application.objects.filter(vacant_topic_id=pk, applicant=request.user).exists():
                response['valid'] = False
                response['message'] = 'You already applied for this work.'
            else:
                application = Application(vacant_topic_id=pk, applicant=request.user, text=request.POST['text'])
                application.save()
        else:
            response['valid'] = False
            response['errors'] = {}
            for key, value in form.errors.iteritems():
                response['errors'][key] = [error for error in value]
    else:
        response['valid'] = False
        response['message'] = 'Method not allowed.'
    return JsonResponse(response)


@csrf_exempt
def approve_application(request, pk):
    if request.method == 'PUT':
        vacant_topics = VacantTopic.objects.get(id=pk)
        vacant_topics.locked = User.objects.get(id=QueryDict(request.body)['user_id'])
        vacant_topics.save()
    else:
        return JsonResponse({'message': 'Method not allowed.'})
    return JsonResponse({'done': 'done'})


class VacantTopicsList(View):
    template_name = 'vacant_topics_list.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


def ajax_vacant_topics_list(request):
    vacant_topics = VacantTopic.objects.exclude(for_student__isnull=False)
    ajax_response = {"sEcho": '', "aaData": [], 'iTotalRecords': vacant_topics.count(),
                     'iTotalDisplayRecords': vacant_topics.count()}
    start = int(request.GET['iDisplayStart'])
    length = int(request.GET['iDisplayLength'])
    for vacant_topic in vacant_topics[start:start + length]:
        locked = False
        if vacant_topic.locked:
            locked = True
        ajax_response['aaData'].append([(vacant_topic.id, vacant_topic.subject), vacant_topic.applications.count(),
                                        locked])
    return JsonResponse(ajax_response)
