from django import forms


class VacantTopicForm(forms.Form):
    subject = forms.CharField(label='Subject', max_length=50)
    year = forms.IntegerField(required=False)
    annotation = forms.CharField(label='Annotation', required=True)


class ApplicationForm(forms.Form):
    text = forms.CharField(label='Text')
