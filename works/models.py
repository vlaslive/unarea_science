from django.contrib.auth.models import User
from django.db import models


class VacantTopic(models.Model):
    subject = models.CharField(max_length=50)
    year = models.IntegerField(default=0)
    annotation = models.TextField(default='')
    user = models.ForeignKey(User, related_name='vacant_topics')
    locked = models.ForeignKey(User, default=None, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    for_student = models.ForeignKey(User, default=None, null=True, related_name='invites')

    def __str__(self):
        return '%s' % self.subject


class Application(models.Model):
    vacant_topic = models.ForeignKey(VacantTopic, related_name='applications')
    applicant = models.ForeignKey(User)
    text = models.TextField(default='', blank=False)
    created_at = models.DateTimeField(auto_now_add=True)
