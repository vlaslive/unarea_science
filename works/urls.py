from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^add_vacant_topic$', views.add_vacant_topic, name='add_vacant_topic'),
    url(r'^ajax_vacant_topics_list', views.ajax_vacant_topics_list, name='ajax_vacant_topics_list'),
    url(r'^vacant_topics_list', views.VacantTopicsList.as_view(), name='vacant_topics_list'),
    url(r'^vacant_topic/(?P<pk>\d+)$', views.VacantTopicPage.as_view(), name='vacant_topic'),
    url(r'^add_application/(?P<pk>\d+)$', views.add_application, name='add_application'),
    url(r'^approve_application/(?P<pk>\d+)$', views.approve_application, name='approve_application'),
    url(r'^vacant_students$', views.vacant_students, name='vacant_students')
]
